project_name: "block-keboola-mrr-config"

################ Constants ################

constant: CONFIG_PROJECT_NAME {
  value: "block-keboola-mrr-config"
}

constant: CONNECTION {
  value: "keboola_block_mrr_salesforce"
}

constant: SCHEMA_NAME {
  value: "WORKSPACE_544085999"
}
